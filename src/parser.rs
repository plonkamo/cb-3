use crate::{C1Lexer, C1Token, ParseResult};

pub struct C1Parser<'a> {
    lexer: C1Lexer<'a>,
}

impl<'a> C1Parser<'a> {
    pub fn parse(text: &'a str) -> ParseResult {
        let mut parser: C1Parser = C1Parser { lexer: C1Lexer::new(text) };
        return parser.program();
        
    }

    fn error_string_generator(&self) -> String {
        return format!("Error on line: {}", self.lexer.current_line_number().unwrap());
    } 

    fn eat(&mut self) {
        self.lexer.eat();
    }

    fn check_and_eat_token(&mut self, token: Option<C1Token>) -> ParseResult {
        if self.lexer.current_token() == token {
            self.eat();
            return Result::Ok(());
        }
        return Result::Err(self.error_string_generator());
        
    }

    fn current_matches(&mut self, token: C1Token) -> bool {
        self.lexer.current_token() == Some(token)
    }

    fn next_matches(&mut self, token: C1Token) -> bool {
        self.lexer.peek_token() == Some(token)
    }


    fn program(&mut self) -> ParseResult{
        self.program_alt()?;
        self.check_and_eat_token(None)
    }
    
    fn program_alt(&mut self) -> ParseResult {
        if self.lexer.current_token() == None {
            return Result::Ok(());
        }
        self.functiondefinition()?;
        self.program_alt()
    }

    fn functiondefinition(&mut self) -> ParseResult {
        self.type_statement()?;
        self.check_and_eat_token(Some(C1Token::Identifier))?;
        self.check_and_eat_token(Some(C1Token::LeftParenthesis))?;
        self.check_and_eat_token(Some(C1Token::RightParenthesis))?;
        self.check_and_eat_token(Some(C1Token::LeftBrace))?;
        self.statementlist()?;
        self.check_and_eat_token(Some(C1Token::RightBrace))
    }

    fn type_statement(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(C1Token::KwBoolean) | Some(C1Token::KwFloat) | Some(C1Token::KwInt) | Some(C1Token::KwVoid) => {
                self.eat(); 
                return Result::Ok(());
            },
            _ => return Result::Err(self.error_string_generator()),
        };
    }

    fn statementlist(&mut self) -> ParseResult {
        if self.lexer.current_token() == Some(C1Token::RightBrace) {
            return Result::Ok(());
        }
        self.block()?;
        self.statementlist()
    }

    fn block(&mut self) -> ParseResult {
        if self.current_matches(C1Token::LeftBrace) {
            self.eat();
            self.statementlist()?;
            return self.check_and_eat_token(Some(C1Token::RightBrace));
        }
        self.statement()        
    }

    fn statement(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(C1Token::KwIf) => return self.ifstatement(),
            Some(C1Token::KwReturn) => {
                self.returnstatement()?;
                return self.check_and_eat_token(Some(C1Token::Semicolon));},
            Some(C1Token::KwPrintf) => {
                self.printfstatement()?; 
                return self.check_and_eat_token(Some(C1Token::Semicolon));},
            Some(C1Token::Identifier) => {
                if self.next_matches(C1Token::LeftParenthesis) {
                    self.functioncall()?;
                    return self.check_and_eat_token(Some(C1Token::Semicolon));
                } else if self.next_matches(C1Token::Assign) {
                    self.statassignment()?;
                    return self.check_and_eat_token(Some(C1Token::Semicolon));
                }
                return Result::Err(self.error_string_generator());
            },
            _ => return Result::Err(self.error_string_generator()),
        };
    }

    fn ifstatement(&mut self) -> ParseResult {
        self.check_and_eat_token(Some(C1Token::KwIf))?;
        self.check_and_eat_token(Some(C1Token::LeftParenthesis))?;
        self.assignment()?;
        self.check_and_eat_token(Some(C1Token::RightParenthesis))?;
        self.block()
    }

    fn returnstatement(&mut self) -> ParseResult {
        self.check_and_eat_token(Some(C1Token::KwReturn))?;
        self.returnstatement_alt()
    }

    fn returnstatement_alt(&mut self) -> ParseResult {
        if self.current_matches(C1Token::Semicolon) {
            return Result::Ok(());
        };
        self.assignment()
    }

    fn printfstatement(&mut self) -> ParseResult {
        self.check_and_eat_token(Some(C1Token::KwPrintf))?;
        self.check_and_eat_token(Some(C1Token::LeftParenthesis))?;
        self.assignment()?;
        self.check_and_eat_token(Some(C1Token::RightParenthesis))
    }

    fn statassignment(&mut self) -> ParseResult {
        self.check_and_eat_token(Some(C1Token::Identifier))?;
        self.check_and_eat_token(Some(C1Token::Assign))?;
        self.assignment()
    }

    fn functioncall(&mut self) -> ParseResult {
        self.check_and_eat_token(Some(C1Token::Identifier))?;
        self.check_and_eat_token(Some(C1Token::LeftParenthesis))?;
        self.check_and_eat_token(Some(C1Token::RightParenthesis))
    }

    fn assignment(&mut self) -> ParseResult {
        if self.current_matches(C1Token::Identifier) {
            if self.next_matches(C1Token::Assign) {
                return self.assignment();
            }
        }
        self.expr()
    }

    fn expr(&mut self) -> ParseResult {
        self.simpexpr()?;
        self.expr_alt()
    }

    fn expr_alt(&mut self) -> ParseResult {
        if self.current_matches(C1Token::Semicolon) || self.current_matches(C1Token::RightParenthesis) {
            return Result::Ok(());
        }
        self.compare()?;
        self.simpexpr()
    }

    fn compare(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(C1Token::Equal) | Some(C1Token::NotEqual) | Some(C1Token::Less) | Some(C1Token::LessEqual) | Some(C1Token::GreaterEqual) | Some(C1Token::Greater) => {self.eat(); return Result::Ok(());},
            _ => return Result::Err(self.error_string_generator()),
        }
    }

    fn simpexpr(&mut self) -> ParseResult {
        self.maybeminus()?;
        self.term()?;
        self.simpexpr_alt()
    }

    fn simpexpr_alt(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(C1Token::Plus) | Some(C1Token::Minus) | Some(C1Token::Or) => {
                self.lowop()?;
                self.term()?;
                self.simpexpr_alt()
            },
            _ => Result::Ok(()),
        }
    }

    fn maybeminus(&mut self) -> ParseResult {
        if self.current_matches(C1Token::Minus) {
            self.eat();
        };
        return Result::Ok(());
    }

    fn lowop(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(C1Token::Plus) | Some(C1Token::Minus) | Some(C1Token::Or) => {
                self.eat(); 
                return Result::Ok(());
            },
            _ => return Result::Err(self.error_string_generator()),
        };
    }

    fn term(&mut self) -> ParseResult {
        self.factor()?;
        self.term_alt()
    }

    fn term_alt(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(C1Token::Asterisk) | Some(C1Token::Slash) | Some(C1Token::And) => {
                self.highop()?;
                self.factor()?;
                return self.term_alt();
            },
            _ => return Result::Ok(()),
        }
    }

    fn highop(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(C1Token::Asterisk) | Some(C1Token::Slash) | Some(C1Token::And) => {
                self.eat(); 
                return Result::Ok(());
            },
            _ => return Result::Err(self.error_string_generator()),
        };
    }

    fn factor(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(C1Token::ConstFloat) | Some(C1Token::ConstInt) | Some(C1Token::ConstBoolean) => {
                self.eat();
                return Result::Ok(());},
            Some(C1Token::Identifier) => {
                if self.next_matches(C1Token::LeftParenthesis) {
                    return self.functioncall();
                }
                self.eat();
                return Result::Ok(());
            },
            Some(C1Token::LeftParenthesis) => {
                self.eat();
                self.assignment()?;
                return self.check_and_eat_token(Some(C1Token::RightParenthesis));
            } ,
            _ => return Result::Err(self.error_string_generator()),
        }
    }
}

/*
*
* program               := program' EOF
* program'              := functiondefinition program' 
                            | epsilon
*
* functiondefinition    := type <ID> "(" ")" "{" statementlist "}"
* 
* functioncall          := <ID> "(" ")"
*
* statementlist         := block statementlist 
                            | epsilon
* 
* block                 := "{" statementlist "}" | statement
* 
* statement             := ifstatement 
                            | returnstatement ";" 
                            | printf ";" 
                            | statassignment ";" 
                            | functioncall ";"
*
* ifstatement           := <KW_IF> "(" assignment ")" block
*
* returnstatement       := <KW_RETURN> returnstatement'
*
* returnstatement'       := assignment 
                            | epsilon
* 
* printf                := <KW_PRINTF> "(" assignment ")"
* 
* type                  := <KW_BOOLEAN> 
                            | <KW_FLOAT> 
                            | <KW_INT> 
                            | <KW_VOID>
*
* statassignment        := <ID> "=" assignment 
*
* assignment            := ( <ID> "=" assignment ) 
                            | expr 
*
* expr                  := simpexpr expr'
*
* expr'                 := compare simpexpr | epsilon
*
* compare               := "==" | "!=" | "<=" | ">=" | "<" | ">"
*
* simpexpr              := maybeminus term simpexpr'
* 
* maybeminus            := "-" |epsilon
*
* simpexpr'             := lowop term simpexpr' | epsilon
*
* lowop                 := "+" | "-" | "||"
*
* term                  := factor term'
*
* term'                 := highop factor term' | epsilon
*
* highop                := "*" | "/" | "&&"
*
* factor                := <CONST_INT> | <CONST_FLOAT> | <CONST_BOOLEAN> | functioncall | <ID> | "(" assignment ")"
*
*/